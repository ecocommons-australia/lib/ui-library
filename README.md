# @ecocommons-australia/ui-library

EcoCommons common UI library designed for Next.js projects

## Key Features

### Header, Footer
Platform wide header, navigation and login status

### Row, Col, FixedContainer
Wrappers for Blueprint layout elements with standardized margins and styles.

### Theme
Abstraction for branding, feature switches and relayout of 'themed' elements. See `Theme.tsx`.

Can be implemented in an application by overriding the default theme `theme/theme.tsx`

For more info see README.Theme.md

### Environment helpers
Standardized functions that provide urls to other microservices.

### Interfaces
TODO

## Installation

```
npm i

# or

yarn
```

## Peer dependencies

* `@blueprintjs/core` 3
* `classnames` 2.2
* `next` 10
* `react` 16+
* `react-grid-system` 7.1

NOTE: You may need to install peer dependencies yourself if you are not using
newer versions of NPM.

## Usage

You import components as you normally do:

```js
// e.g. for <Header /> component
import { Header } from "@ecocommons-australia/ui-library";
```

## Compiling

Run `npm run build`.

## Storybook

### Development server

Run `npm run storybook` to spin up a live Storybook instance on your local
machine.

### Building

Run `npm run build-storybook`. Output is located in `./storybook-static` by
default. If some other directory is desired, use `npm run build-storybook -- -o /path/to/output`.

### Publishing

Run tag a new release

```
npm version x.x.x
git push --tags
```
