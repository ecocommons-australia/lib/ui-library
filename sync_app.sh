#!/bin/bash
# Given npm link is a complete struggle with React hooks, 
# hack an alternative way to test lib builds locally.
# https://legacy.reactjs.org/warnings/invalid-hook-call-warning.html#duplicate-react
#
# Example use (run from app dir):
# ../ui-library/sync_app.sh $(pwd) && npm run dev

set -e
[[ -z $1 ]] && printf "Usage: sync_app.sh APPLICATION_ROOT_DIR\n\n" && exit 1

APP_DIR=$1
LIB_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

cd $LIB_DIR
npm run build

cp -vrf $LIB_DIR/dist/index* $APP_DIR/node_modules/@ecocommons-australia/ui-library/dist/
rm -rf $APP_DIR/.next/cache 

echo "Please restart $1"
