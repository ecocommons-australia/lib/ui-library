import { useEffect, useState } from "react";
import getConfig from "next/config";

import { OverlayToaster, Toaster, Position, Intent, ToastProps } from "@blueprintjs/core";

const config = getConfig();

export interface Props {
    /** 
     * The Keycloak token used for verifying user info for receving notification.
     */
    token: string;
}

enum JobStatus {
    SUCCESS = 'SUCCESS',
    FAILED = 'FAILED'
}

export function Notification({
    token,
}: Props): JSX.Element | null {
    const [AppToaster, setAppToaster] = useState<Promise<Toaster>>(new Promise(() => {}));
    const jobManagerApiUrl: string | undefined = config.publicRuntimeConfig?.NEXT_PUBLIC_JOB_MANAGER_BACKEND_SERVER_URL;
    const jobManagerUiUrl: string | undefined = config.publicRuntimeConfig?.NEXT_PUBLIC_JOB_MANAGER_URL;

    const showToast = async (props: ToastProps) => {
        (await AppToaster).show(props);
    }

    useEffect(() => {
        if (typeof window !== "undefined" && typeof document !== "undefined") {
            setAppToaster(OverlayToaster.createAsync({
                position: Position.TOP_RIGHT,
            }));
        }
    }, []);

    useEffect(() => {
        if (jobManagerApiUrl && jobManagerUiUrl) {
            // Create WebSocket connection
            const socketUrl = `${jobManagerApiUrl}/ws/jobs/`;
            const socket = new WebSocket(socketUrl);

            // Send the keycloak token to authenticate
            socket.onopen = () => {
                socket.send(JSON.stringify({ token }));
            };

            // Handle incoming messages
            socket.onmessage = (event: MessageEvent) => {
                const data = JSON.parse(event.data);
                if (data.message && data.job_uuid && data.jobreq_uuid && data.status) {
                    showToast({
                        message: data.message,
                        intent: data.status === JobStatus.SUCCESS ? Intent.SUCCESS : Intent.WARNING,
                        timeout: 6000,
                        action: {
                            href: `${jobManagerUiUrl}/job/${data.jobreq_uuid}`,
                            target: "_blank",
                            text: <strong>View Result</strong>,
                        },
                    });
                }
            };

            socket.onerror = (error) => {
                console.error('WebSocket Error:', error);
            };

            return () => {
                socket.close();
            };
        }

        return undefined;
    }, [jobManagerApiUrl, jobManagerUiUrl, AppToaster]);

    return null;
}
