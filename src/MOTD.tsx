import { Callout } from "@blueprintjs/core";
import { useEffect, useState } from "react";
import getConfig from "next/config";

const config = getConfig();

/**
 * Message of the day
 */
export function MOTD() {
    const [motd, setMotd] = useState<string | null>(null);
    const motdURL = config.publicRuntimeConfig?.NEXT_PUBLIC_MOTD_URL;

    useEffect(() => {
        async function fetchMOTD() {
            if (!motdURL) {
                return;
            }

            try {
                const response = await fetch(motdURL);
                if (!response.ok) {
                    throw new Error(
                        `Error fetching MOTD: ${response.statusText}`
                    );
                }
                const data = await response.text();
                setMotd(data);
            } catch (error) {
                console.error("Failed to fetch MOTD:", error);
            }
        }

        fetchMOTD();
    }, [motdURL]);

    if (!motd || String(motd).trim() === "") {
        return null;
    }

    return (
        <div
            style={{
                display: "flex",
                justifyContent: "center",
                backgroundColor: "rgba(45, 114, 210, 0.1)"
            }}
        >
            <Callout
                title="Service notification"
                intent="primary"
                style={{
                    display: "flex",
                    width: "auto",
                    backgroundColor: "transparent",
                    marginTop: "-5px",
                    marginBottom: "-5px",
                    fontSize: "95%"
                }}
            >
                <div
                    style={{ marginLeft: "10px" }}
                    dangerouslySetInnerHTML={{ __html: motd }}
                />
            </Callout>
        </div>
    );
}
