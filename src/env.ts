import getConfig from "next/config";

const config = getConfig();

export const IS_DEVELOPMENT = config.publicRuntimeConfig.NEXT_PUBLIC_DEPLOYMENT !== "production";

export function getEnvLabel(): string | undefined {
    return config.publicRuntimeConfig.NEXT_PUBLIC_DEPLOYMENT;
}

export function getEnvShortLabel(): string | undefined {
    switch(config.publicRuntimeConfig.NEXT_PUBLIC_DEPLOYMENT){
        case 'production':
            return 'prod';
        case 'test':
            return 'test';
        case 'development':
            return 'dev';
        default:
            return undefined;
    }
}

export function getKeycloakAuthParameters() {
    return {
        url: config.publicRuntimeConfig
            .NEXT_PUBLIC_KEYCLOAK_AUTH_URL as string,
        realm: config.publicRuntimeConfig
            .NEXT_PUBLIC_KEYCLOAK_AUTH_REALM as string,
        clientId: config.publicRuntimeConfig
            .NEXT_PUBLIC_KEYCLOAK_AUTH_CLIENT_ID as string
    };
}

export function getDataExplorerUrl() {
    return config.publicRuntimeConfig
        .NEXT_PUBLIC_DATA_EXPLORER_URL as string;
}

export function getWorkspaceUrl() {
    return config.publicRuntimeConfig
        .NEXT_PUBLIC_WORKSPACE_URL as string;
}

export function getDataManagerUrl() {
    return config.publicRuntimeConfig
        .NEXT_PUBLIC_DATA_MANAGER_URL as string;
}

export function getWorkflowBccvlUrl() {
    return config.publicRuntimeConfig
        .NEXT_PUBLIC_ANALYSIS_TOOLS_MODELLING_WIZARDS_BCCVL_URL as string;
}

export function getWorkflowUrl() {
    return config.publicRuntimeConfig
        .NEXT_PUBLIC_WORKFLOW_URL as string;
}

export function getJobManagerBackendServerUrl() {
    return config.publicRuntimeConfig
        .NEXT_PUBLIC_JOB_MANAGER_BACKEND_SERVER_URL as string;
}

export function getDataExplorerBackendServerUrl() {
    return config.publicRuntimeConfig.NEXT_PUBLIC_DATA_EXPLORER_BACKEND_SERVER_URL as string;
}

export function getDataManagerBackendServerUrl() {
    return config.publicRuntimeConfig.NEXT_PUBLIC_DATA_MANAGER_BACKEND_SERVER_URL as string;
}

export function getWorkflowManagerBackendServerUrl() {
    return config.publicRuntimeConfig
        .NEXT_PUBLIC_WORKFLOW_MANAGER_BACKEND_SERVER_URL as string;
}

export function getUserManagementBackendServerUrl() {
    return config.publicRuntimeConfig
        .NEXT_PUBLIC_USER_MANAGEMENT_BACKEND_SERVER_URL as string;
}

export function getAnalysisHubUrl() {
    return config.publicRuntimeConfig
        .NEXT_PUBLIC_UI_LIBRARY_HEADER_ECOCOMMONS_ANALYSIS_HUB as string;
}

export function getGoogleAnalyticsTrackingId() {
    return config.publicRuntimeConfig
        .NEXT_PUBLIC_GOOGLE_ANALYTICS_TRACKING_ID as string;
}