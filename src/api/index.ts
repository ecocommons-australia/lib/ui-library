import axios, {
    AxiosInstance,
    AxiosRequestConfig,
    CancelTokenSource,
    AxiosRequestHeaders
} from "axios";

import { KeycloakInstance } from "../interfaces/Keycloak";

/** 
 * Helper to determine if a string is a UUID
 */
export function isUUID(value: string | undefined): boolean {
    const uuidRegex = /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i;
    return String(value).match(uuidRegex) !== null;
}

export class API {
    protected readonly axios: AxiosInstance;

    constructor(
        public readonly serverBaseUrl: string,
        /** Getter function that retrieves the latest known Keycloak object */
        protected readonly keycloakInstance: () => KeycloakInstance | undefined
    ) {
        const { axiosInstance } = this.initNewAxiosInstance();

        this.axios = axiosInstance;
    }

    public getAuthorizationHeaderValue() {
        const token = this.keycloakInstance()?.token;

        if (token === undefined) {
            return undefined;
        }

        return `Bearer ${token}`;
    }

    protected initNewAxiosInstance() {
        const axiosInstance = axios.create({
            baseURL: this.serverBaseUrl,
        });

        const injectAuthHeaderInterceptor =
            axiosInstance.interceptors.request.use((requestConfig) => {
                const authHeader = this.getAuthorizationHeaderValue();

                // If no auth value available, just pass request through
                if (authHeader === undefined) {
                    return requestConfig;
                }

                // Otherwise inject the auth header
                return {
                    ...requestConfig,
                    headers: {
                        ...requestConfig.headers,
                        Authorization: authHeader,
                    } as AxiosRequestHeaders,
                };
            });

        return { axiosInstance, injectAuthHeaderInterceptor };
    }

    protected getNewAxiosCancellationToken() {
        return axios.CancelToken.source();
    }

    protected xhrGet<T>(
        url: string,
        cancelTokenSource?: CancelTokenSource,
        options: AxiosRequestConfig = {}
    ) {
        const cancellationToken =
            cancelTokenSource ?? this.getNewAxiosCancellationToken();
        const axiosPromise = this.axios.get<T>(url, {
            ...options,
            cancelToken: cancellationToken.token,
        });
        const promise = axiosPromise.then((res) => res.data);
        return { promise, cancellationToken, axiosPromise };
    }

    protected xhrPost<T>(
        url: string,
        data: unknown,
        cancelTokenSource?: CancelTokenSource,
        options: AxiosRequestConfig = {}
    ) {
        const cancellationToken =
            cancelTokenSource ?? this.getNewAxiosCancellationToken();
        const axiosPromise = this.axios.post<T>(url, data, {
            ...options,
            cancelToken: cancellationToken.token,
        });
        const promise = axiosPromise.then((res) => res.data);
        return { promise, cancellationToken, axiosPromise };
    }

    protected xhrPatch<T>(
        url: string, 
        data: unknown,
        cancelTokenSource?: CancelTokenSource,
        options: AxiosRequestConfig = {}
        ) {
        const cancellationToken =
            cancelTokenSource ?? this.getNewAxiosCancellationToken();
        const axiosPromise = this.axios.patch<T>(url, data, {
            ...options,
            cancelToken: cancellationToken.token,
        });
        const promise = axiosPromise.then((res) => res.data);
        return { promise, cancellationToken, axiosPromise };
    }

    protected xhrDelete<T>(
        url: string,
        cancelTokenSource?: CancelTokenSource,
        options: AxiosRequestConfig = {}
        ) {
        const cancellationToken =
            cancelTokenSource ?? this.getNewAxiosCancellationToken();
        const axiosPromise = this.axios.delete<T>(url, {
            ...options,
            cancelToken: cancellationToken.token,
        });
        const promise = axiosPromise.then((res) => res.data);
        return { promise, cancellationToken, axiosPromise };
    } 
}
