import React, { createContext, useContext, useState, Dispatch, SetStateAction, ReactElement } from "react";

type AlertType = 
  'unauthorised-alert' |
  'forbidden-alert' |
  'not-found-alert' |
  'bad-req-alert' |
  'server-error-alert' |
  'error-alert'

interface Alert {
    type: AlertType;
    message?: string | ReactElement;
    debug?: object;
}

interface AlertContext {
    alert: Alert | undefined;
    setAlert: Dispatch<SetStateAction<Alert | undefined>>;
}

export const AlertContext = createContext<AlertContext>({} as AlertContext);

export const AlertProvider = ({ children }: React.PropsWithChildren<{}>) => {
  const [alert, setAlert] = useState<Alert | undefined>(undefined);

  return (
    <AlertContext.Provider value={{alert, setAlert}}>
      { children }
    </AlertContext.Provider>
  )
}

export function useAlert() {
  return useContext(AlertContext);
}