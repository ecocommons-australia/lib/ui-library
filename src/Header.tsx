import { Icon } from "@blueprintjs/core";
import { ReactNode, useMemo } from "react";
import { Col, Row } from "react-grid-system";
import classnames from "classnames";

import Link from "next/link";

import { FixedContainer } from "./FixedContainer";
import { BuildID } from "./BuildID";
import { MOTD } from "./MOTD";

import { IHeaderTabLabels } from "./interfaces/ThemableComponents";
import { Urls } from "./constants/Urls";
import styles from "./styles/Header.module.css";
import logo from "./images/EcoCommons Logo.png";
import { useTheme } from "./Theme";
import { Notification } from "./Notification";


export enum NavLinkId {
    WORKSPACE = "workspace",
    DATASETS = "datasets",
    ANALYSIS_HUB = "analysis-hub",
    HELP = "help"
}

export interface Props {
    /** Tab to be rendered as active */
    activeTab?: Omit<NavLinkId, "HELP">;
    /** Values for tab links */
    tabLinks: typeof Urls;
    /** Links to appear underneath the main header row */
    subBarLinks?: readonly {
        /** Arbitrary unique key to identify link */
        key: string;
        /** Href for link */
        href: string;
        /** Link content */
        label: ReactNode;
        /** Alignment */
        align?: "left" | "right" | string;
    }[];
    /** The active sub bar link's key */
    subBarActiveKey?: string;
    /** Node (usually button) to be placed in the sign in/out area */
    signInOutButton?: ReactNode;
    /**
     * Whether to render a yellow striped background (e.g. to indicate special
     * or abnormal situations like a staging deploy)
     */
    useYellowStripedBackground?: boolean;
    /** 
     * The Keycloak token used for verifying user info for receving notification.
     * This token is optional and should be provided if the application requires 
     * notification functionality.
     */
    keycloakToken?: string;
}

/**
 * The default EcoCommons header component across multiple sites.
 */
export function Header({
    activeTab,
    tabLinks,
    subBarLinks,
    subBarActiveKey,
    signInOutButton,
    keycloakToken,
    useYellowStripedBackground = false,
}: Props) {

    const { getThemeValue, mergeStyles } = useTheme();
    
    const linkUrls = useMemo(
        () => ({
            'root': tabLinks?.ECOCOMMONS_ROOT,
            'workspace': tabLinks?.ECOCOMMONS_WORKSPACE,
            'datasets': tabLinks?.ECOCOMMONS_DATASETS,
            'analysis-hub': tabLinks?.ECOCOMMONS_ANALYSIS_HUB,
            'help': tabLinks?.ECOCOMMONS_SUPPORT,
        }),
        [tabLinks]
    );

    const linkLabels: IHeaderTabLabels = 
        getThemeValue('Object::Platform.HeaderTabLabels') ?? 
        { 
            'workspace': 'Workspace',
            'datasets': 'Datasets',
            'analysis-hub': 'Analysis Hub',
            'help': 'Help'
        };

    // Optional alternative help component, eg. menu
    const HelpComponent = getThemeValue("Component::Platform.HeaderHelp");

    const themedStyles = mergeStyles(styles, "Styles::Header");

    const organisation =
        getThemeValue("String::Platform.Organisation") ?? "EcoCommons Australia";

    const logoImagePath =
        getThemeValue("String::Platform.LogoImagePath") ?? logo;

    return (
        <>
            <MOTD />
            {keycloakToken && <Notification token={keycloakToken} />}
            <div
                className={classnames({
                    [themedStyles.headerContainer]: true,
                    [themedStyles.yellowStripedBackground]: useYellowStripedBackground,
                })}
                data-active-tab={activeTab}
                data-cy="header"
            >
                <div data-cy="header-nav-bar">
                    <BuildID />
                    <FixedContainer>
                        <Row align="center" className={themedStyles.navBar}>
                            <Col sm={3}>
                                <Link legacyBehavior href={linkUrls.root}>
                                    <a>
                                        <img
                                            src={logoImagePath}
                                            title={organisation}
                                            alt={organisation}
                                            className={themedStyles.logo}
                                        />
                                    </a>
                                </Link>
                            </Col>
                            <Col sm={9}>
                                <div className={themedStyles.navAndUserBlock}>
                                    <Row justify="between" align="center">
                                        <ul className={themedStyles.navLinks}>
                                            <li data-tab="workspace" key="workspace">
                                                <Link legacyBehavior href={linkUrls[NavLinkId.WORKSPACE]}>
                                                    <a>{linkLabels[NavLinkId.WORKSPACE]}</a>
                                                </Link>
                                            </li>
                                            
                                            <li data-tab="datasets" key="datasets">
                                                <Link legacyBehavior href={linkUrls[NavLinkId.DATASETS]}>
                                                    <a>{linkLabels[NavLinkId.DATASETS]}</a>
                                                </Link>
                                            </li>

                                            <li data-tab="analysis-hub" key="analysis-hub">
                                                <Link legacyBehavior href={linkUrls[NavLinkId.ANALYSIS_HUB]}>
                                                    <a>{linkLabels[NavLinkId.ANALYSIS_HUB]}</a>
                                                </Link>
                                            </li>
                                            
                                            { linkUrls[NavLinkId.HELP] && 
                                                <li data-tab="help" key="help">
                                                    <Link legacyBehavior href={linkUrls[NavLinkId.HELP]}>
                                                        <a target="_blank">
                                                            <Icon
                                                                icon="help"
                                                                style={{
                                                                    marginRight: "0.2em"
                                                                }}
                                                            />
                                                            {linkLabels[NavLinkId.HELP]}
                                                        </a>
                                                    </Link>
                                                </li>
                                            }
                                            
                                        </ul>
                                        {HelpComponent && <div><HelpComponent/></div>}
                                        <div>{signInOutButton}</div>
                                    </Row>
                                </div>
                            </Col>
                        </Row>
                    </FixedContainer>
                </div>
                {subBarLinks && (
                    <div className={themedStyles.subBar} data-cy="header-sub-bar">
                        <FixedContainer>
                            {subBarLinks.map(({ key, href, label, align }) => (
                                <Link legacyBehavior key={key} href={href}>
                                    <a
                                        className={
                                            key === subBarActiveKey
                                                ? themedStyles.subBarLinkActive
                                                : undefined
                                        }
                                        style={{
                                            float: align === "right" ? "right" : "none"
                                        }}
                                    >
                                        {label}
                                    </a>
                                </Link>
                            ))}
                        </FixedContainer>
                    </div>
                )}
            </div>
        </>
    );
}
