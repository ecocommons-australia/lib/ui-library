import getConfig from "next/config";

const config = getConfig();

export function BuildID() {

    const buildId = config.publicRuntimeConfig?.NEXT_PUBLIC_BUILD_ID;
    const isDev = config.publicRuntimeConfig?.NEXT_PUBLIC_DEPLOYMENT !== "production";

    if (isDev === true && buildId !== undefined){
        return <div 
            style={{
                position: 'absolute',
                top: 0,
                left: 0,
                fontSize: '90%',
                background: 'white',
                padding: '0.2rem',
                opacity: 0.85  
            }}
            >
            {buildId}
            </div>
        }
    return <></>;
};
