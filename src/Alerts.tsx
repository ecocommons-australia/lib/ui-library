import { useRouter } from "next/router";
import { Alert, Intent, H3 } from "@blueprintjs/core";
import { useAlert } from "./hooks/Alert";

export interface Props {
    loginUrl?: string | undefined;
}

export function Alerts({
    loginUrl
}: Props) {

    const router = useRouter();

    const { alert, setAlert } = useAlert();

    return (
        <>
        <Alert
            data-cy="session-alert"
            icon="history"
            intent={Intent.DANGER}
            isOpen={alert?.type === 'unauthorised-alert'}
            onConfirm={() => loginUrl && router.push(loginUrl)}
            onCancel={() => setAlert(undefined)}
            confirmButtonText="Sign in"
            cancelButtonText="Dismiss"
            style={{'minWidth': '40rem'}}
        >
            <H3>Session expired</H3>
            <p>
                Your session has timed out due to inactivity. Any unsaved work may be lost.
            </p>
            <p>
                Please sign in again to continue.
            </p>
        </Alert>

        <Alert
            data-cy="forbidden-alert"
            confirmButtonText="Close"
            icon="error"
            intent={Intent.DANGER}
            isOpen={alert?.type === 'forbidden-alert'}
            onConfirm={() => setAlert(undefined)}
            style={{'minWidth': '40rem'}}
        >
            <H3>Operation not permitted</H3>
            <p>
                You are not permitted to perform the requested operation.
            </p>
            <hr/>
            <p>
                {alert?.message}
            </p>
        </Alert>

        <Alert
            data-cy="not-found-alert"
            confirmButtonText="Close"
            icon="error"
            intent={Intent.DANGER}
            isOpen={alert?.type === 'not-found-alert'}
            onConfirm={() => setAlert(undefined)}
            style={{'minWidth': '40rem'}}
        >
            <H3>Resource not available</H3>
            <p>
                A requested resource is not available.
            </p>
            <hr/>
            <p>
                {alert?.message}
            </p>
        </Alert>

        <Alert
            data-cy="error-alert"
            confirmButtonText="Close"
            icon="error"
            intent={Intent.DANGER}
            isOpen={[
                'bad-req-alert', 
                'server-error-alert', 
                'error-alert'
                ].includes(alert?.type as string)}
            onConfirm={() => setAlert(undefined)}
            style={{'minWidth': '40rem'}}
        >
            <H3>Error</H3>
            <p>
                Something went wrong with the performed action. This may indicate a problem with data or an application issue.
            </p>
            <hr/>
            <p>
                {alert?.message}
            </p>
        </Alert>

        </>
    );
}
