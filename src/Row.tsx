import classnames from "classnames";
import { Row as RGSRow } from "react-grid-system";
import { ComponentPropsWithoutRef } from "react";

import style from "./Row.module.css";

interface CustomProps {
    /** Whether to disable default margins applied by this library */
    disableDefaultMargins?: boolean;
}

export function Row({
    className,
    disableDefaultMargins = false,
    // Default to 16 gutter between <Col /> children
    gutterWidth = 16,
    ...props
}: ComponentPropsWithoutRef<typeof RGSRow> & CustomProps) {
    // TODO: Restore `ref` passthrough
    return (
        <RGSRow
            className={classnames(
                { [style.row]: !disableDefaultMargins },
                className
            )}
            gutterWidth={gutterWidth}
            {...props}
        />
    );
}
