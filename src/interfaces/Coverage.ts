export interface IAxis {
    num: number;
    stop: number;
    start: number;
}

export interface IAxes {
    x?: IAxis;
    y?: IAxis;
}

export interface ISystem {
    type: string;
    wkt: string;
}

export enum Coordinate {
    X = "x",
    Y = "y",
}

export interface IReferencing {
    coordinates?: Coordinate[];
    type?: string;
    columns?: string[];
    system: ISystem;
}

export interface IDomain {
    type: string;
    domainType: string;
    axes: IAxes;
    referencing: IReferencing[];
}

export interface ILabel {
    en: string;
}

export interface IUnit {
    symbol: ISymbol;
}

export interface ISymbol {
    value: IValue;
}

export interface IValue {
    type: string;
    value: string | IValueValue;
}

export interface IValueValue {
    symbol: IValueSymbol;
}

export interface IValueSymbol {
    type: string;
    value: string;
}

export interface ICategory {
    id: string;
    label: ILabel;
}

export interface IObservedProperty {
    id: string;
    label: ILabel;
    categories?: ICategory[];
}

export interface IParameterData {
    type: string;
    observedProperty: IObservedProperty;
    unit?: IUnit;
    categoryEncoding?: { [key: string]: number };
}

export interface IParameters {
    [parameter: string]: IParameterData;
}

export interface IRanges {}

export interface IExtentWgs84 {
    top: number;
    left: number;
    right: number;
    bottom: number;
}

export interface IAttribution {
    type: string;
    value: string;
}

export interface IDmgrFile {
    url: string;
    tempurl: string;
}

export interface IDmgrCsv {
    url: string;
    tempurl: string;
}

export interface IDmgrTiff {
    [layer: string]: IDmgrTiffLayerData;
}

export interface IRangeAlternates {
    "dmgr:file"?: IDmgrFile;
    "dmgr:csv"?:  IDmgrCsv;
    "dmgr:tiff"?: IDmgrTiff;
}

export interface IDmgrTiffLayerData {
    type: string;
    datatype: string;
    axisNames: string[] | Coordinate[];
    shape: number[];
    "dmgr:datatype": string;
    url: string;
    tempurl: string;
}

