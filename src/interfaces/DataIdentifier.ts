export enum DataIdentifierKeys {
    GENRE = "genre",
    DATA_CATEGORY = "dataCategory",
    MIMETYPE = "mimetype"
}

export enum DataIdentifier {
    // Datasets, Results
    SpatialDatatypeContinuous       = "spatialDataType:Continuous",
    SpatialDatatypeCategorical      = "spatialDataType:Categorical",
    SpatialDatatypePoint            = "spatialDataType:Point",
    SpatialDatatypeShape            = "spatialDataType:Shape",
    SpatialDatatypeUnclassified     = "spatialDataType:Unclassified",

    TimeDomainCurrent               = "timeDomain:Current",
    TimeDomainFuture                = "timeDomain:Future",

    // Datasets: Uploaded
    DataGenreSpatialRaster          = "genre:DataGenreSpatialRaster",
    DataGenreSpatialPoint           = "genre:DataGenreSpatialPoint",
    DataGenreSpatialShape           = "genre:DataGenreSpatialShape",
    DataGenreAspatial               = "genre:DataGenreAspatial",
    DataGenreFile                   = "genre:DataGenreFile",
    DataGenreTemplateSpatial        = "genre:DataGenreTemplateSpatial",

    DataGenreSpeciesBias            = "genre:DataGenreSpeciesBias",
    DataGenreClimateChange          = "genre:DataGenreCC",
    DataGenreEnvironmental          = "genre:DataGenreE",
    DataGenreFutureClimate          = "genre:DataGenreFC",
    DataGenreFutureEnvironmental    = "genre:DataGenreFE",
    DataGenreSpeciesAbsence         = "genre:DataGenreSpeciesAbsence",
    DataGenreSpeciesOccurrence      = "genre:DataGenreSpeciesOccurrence",
    DataGenreSpeciesTraits          = "genre:DataGenreSpeciesTraits",
    DataGenreSpeciesAbsenceCollection = "genre:DataGenreSpeciesAbsenceCollection",
    DataGenreSpeciesOccurrenceCollection = "genre:DataGenreSpeciesOccurrenceCollection",

    // Datasets: Curated (incomplete list..)
    DataCategoryClimate             = "dataCategory:climate",
    DataCategoryEnvironmental       = "dataCategory:environmental",
    DataCategoryTemperature         = "dataCategory:temperature",
    DataCategoryPrecipitation       = "dataCategory:precipitation",
    DataCategoryBioclim             = "dataCategory:bioclim",
    DataCategoryHumanFactors        = "dataCategory:humanFactors",

    // Results: Generic
    DataGenreJobScript                      = "genre:JobScript",
    DataGenreInputParams                    = "genre:InputParams",
    DataGenreLog                            = "genre:DataGenreLog",
    DataGenreMetadata                       = "genre:DataGenreMetadata",

    // Results: SDM, CC, Trait etc..
    DataGenreSpeciesOccurEnv                = "genre:DataGenreSpeciesOccurEnv",              
    DataGenreClimateChangeMetricMap         = "genre:DataGenreClimateChangeMetricMap",
    DataGenreSpeciesAbsenceEnv              = "genre:DataGenreSpeciesAbsenceEnv",
    DataGenreSDMEval                        = "genre:DataGenreSDMEval",
    DataGenreFP_PLOT                        = "genre:DataGenreFP_PLOT",
    DataGenreSDMModel                       = "genre:DataGenreSDMModel",
    DataGenreCP_ENVLOP                      = "genre:DataGenreCP_ENVLOP",
    DataGenreEnsembleResult                 = "genre:DataGenreEnsembleResult",
    DataGenreSDMResult                      = "genre:DataGenreSDMResult",
    DataGenreDataExploration                = "genre:DataGenreDataExploration",
    DataGenreSDMModellingRegion             = "genre:DataGenreSDMModellingRegion",
    DataGenreSTModel                        = "genre:DataGenreSTModel",
    DataGenreClimateChangeMetric            = "genre:DataGenreClimateChangeMetric",
    DataGenreSDMModelResult                 = "genre:DataGenreSDMModelResult",
    DataGenreFP                             = "genre:DataGenreFP",
    DataGenreSTResult                       = "genre:DataGenreSTResult",
    DataGenreCP                             = "genre:DataGenreCP",

    // Results: Biosecurity
    DataGenreRiskMappingResult                  = "genre:DataGenreRiskMappingResult",
    DataGenreRiskMappingResultLog               = "genre:DataGenreRiskMappingResultLog",
    DataGenreBSEstablishmentLikelihood          = "genre:DataGenreBSEstablishmentLikelihood",
    DataGenreBSRiskMappingSuitability           = "genre:DataGenreBSRiskMappingSuitability",
    DataGenreBSRiskMappingLikelihood            = "genre:DataGenreBSRiskMappingLikelihood",
    DataGenreBSRiskMappingPathway               = "genre:DataGenreBSRiskMappingPathway",
    DataGenreBSSurveillanceAllocationsSpatial   = "genre:DataGenreBSSurveillanceAllocationsSpatial",
    DataGenreDispersal                          = "genre:DataGenreDispersal",
    DataGenreDispersalPlot                      = "genre:DataGenreDispersalPlot",
    DataGenreDispersalResult                    = "genre:DataGenreDispersalResult",
    DataGenreBSSurveillanceAllocationSpatial      = "genre:DataGenreBSSurveillanceAllocationSpatial",
    DataGenreBSSurveillanceAllocationCellsSpatial = "genre:DataGenreBSSurveillanceAllocationCellsSpatial",
    DataGenreBSSurveillanceSensitivitySpatial   = "genre:DataGenreBSSurveillanceSensitivitySpatial",
    DataGenreBSSurveillanceDesign               = "genre:DataGenreBSSurveillanceDesign",
    DataGenreBSSurveillanceSummary              = "genre:DataGenreBSSurveillanceSummary",
    DataGenreBSSurveillanceAllocationCoords     = "genre:DataGenreBSSurveillanceAllocationCoords",
    DataGenreBSImpactAnalysisIncursionImpacts   = "genre:DataGenreBSImpactAnalysisIncursionImpacts",
    DataGenreBSImpactAnalysisIncursionMgmtCosts = "genre:DataGenreBSImpactAnalysisIncursionMgmtCosts",
    DataGenreBSImpactAnalysisCombinedImpacts    = "genre:DataGenreBSImpactAnalysisCombinedImpacts",
    DataGenreBSImpactAnalysisTotalCosts         = "genre:DataGenreBSImpactAnalysisTotalCosts",

    // Generic files
    MimetypeImageTiff               = "mimetype:image/tiff" ,
    MimetypeImageGeotiff            = "mimetype:image/geotiff",
    MimetypeTextCsv                 = "mimetype:text/csv",
    MimetypeApplicationCsv          = "mimetype:application/csv",
    MimetypeFileShapefile           = "mimetype:file/zipped-shapefile"
}
