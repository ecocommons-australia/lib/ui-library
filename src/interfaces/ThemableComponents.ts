export interface IHeaderSubBarLink {
	key: string;
	href: string;
	label: string;
}

export interface IHeaderTabLabels {
	"workspace": string;
	"datasets": string;
	"analysis-hub": string;
	"help": string;
}

export interface IWorkflowCard {
	id: string;
	title: string;
	description: string;
	imagePath: string;
	url: string;
}

export type IDataExplorerExploreProviders = Array<string>;

export type ISupportMap = { [key: string]: string };
