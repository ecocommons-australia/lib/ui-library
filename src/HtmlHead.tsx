import Head from "next/head";
import { applyThemeValuesToPropsHoc } from "./Theme";
import { IS_DEVELOPMENT, getEnvShortLabel } from "./env";

export interface Props {
    /** Title to appear in <head /> for webpage */
    title?: string | readonly string[];

    siteName?: string;
}

function _HtmlHead({
    title,
    siteName = "EcoCommons",
}: Props) {
    let titleString: string = "";

    if (title) {
        if (typeof title === "string") {
            titleString = ` - ${title}`;
        } else {
            titleString = title.reduce(
                (str, segment) => str + ` - ${segment}`,
                ""
            );
        }
    }

    const env = IS_DEVELOPMENT ? `[${getEnvShortLabel() ?? ''}]` : ``;

    return (
        <Head>
            <title>{env}{siteName}{titleString}</title>
        </Head>
    );
}

export const HtmlHead = applyThemeValuesToPropsHoc({
    siteName: "String::Platform.Name",
}, _HtmlHead);
