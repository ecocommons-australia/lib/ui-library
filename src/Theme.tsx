import React, { createContext, FunctionComponent, ProviderProps, useCallback, useContext } from "react";
import classnames from "classnames";

export type ThemeConfigCategory<Prefix extends string, Value, Key extends string = string> =
    Partial<Record<`${Prefix}::${Key}`, Value>>;

export type ThemeConfigStringKeys =
    | "Platform.Name"
    | "Platform.LogoImagePath"
    | "Platform.Organisation"
    | "Platform.SupportEmail"
    | "Platform.SupportUrl"
    | "DataExplorer.Explore.Filters"
    | "DataExplorer.Explore.Types"
    | "Workspace.Links"
    | "Workspace.Projects.Description"
    | "Workspace.Results.Description"
    | "Workflow.Projects.Owned.Description"
    | "Workflow.Projects.Shared.Description"


export type ThemeConfigStyleKeys =
    | "Header"
    | "Footer"
    | "DatasetCard";

export type ThemeConfigBooleanKeys =
    | "BccvlJobComposer.EnableBccvlSubBar"
    | "DataExplorer.Explore.AllowUpload"
    | "DataExplorer.Explore.EnableKnowledgeNetwork"

export type ThemeConfigObjectKeys =
    | "Platform.HeaderTabLinks"
    | "Platform.HeaderTabLabels"
    | "Platform.HeaderSubBarLinks"
    | "Platform.HeaderContent"
    | "Platform.FooterContent"
    | "Platform.SupportMap"
    | "BccvlJobComposer.HeaderSubBarLinks"
    | "DataExplorer.Explore.Providers"
    | "DataExplorer.HeaderSubBarLinks"
    | "DataManager.HeaderSubBarLinks"
    | "DataManager.DataImportOccurrenceSources"
    | "DataManager.DataImportOccurrencePageSummary"
    | "AnalysisTools.Workflows"
    | "AnalysisTools.HeaderSubBarLinks"
    | "JobManager.HeaderSubBarLinks"
    | "Workflow.HeaderSubBarLinks"
    | "Workspace.HeaderSubBarLinks"
    | "Workspace.DashboardContent"

export type ThemeConfigNumberKeys =
    | "AnalysisTools.WorkflowCardSize"

export type ThemeConfigImageKeys =
    | "Platform.LogoImagePath"

export type ThemeConfigComponentKeys =
    | "Platform.HeaderHelp"
    | "Workspace.Dashboard"
    | "BccvlJobComposer.IndexPage"

export type ThemeConfigKey =
    ThemeConfigStringKeys
    & ThemeConfigStyleKeys
    & ThemeConfigBooleanKeys
    & ThemeConfigObjectKeys
    & ThemeConfigComponentKeys
    & ThemeConfigNumberKeys;

type ThemeConfigStrings = // Category for strings
    ThemeConfigCategory<
        "String",               // Prefix
        string,                 // Type of the values expected for this category
        ThemeConfigStringKeys   // Permitted keys (optional, see above for definition)
    >;

type ThemeConfigStyleClasses    = ThemeConfigCategory<"StyleClass", string, ThemeConfigStyleKeys>;

type ThemeConfigStyles          = ThemeConfigCategory<"Styles", Record<string, string>, ThemeConfigStyleKeys>;

type ThemeConfigBoolean         = ThemeConfigCategory<"Boolean", boolean, ThemeConfigBooleanKeys>;

type ThemeConfigObject          = ThemeConfigCategory<"Object", {[key: string]: any}, ThemeConfigObjectKeys>;

type ThemeConfigImage           = ThemeConfigCategory<"Image", string, ThemeConfigImageKeys>;

type ThemeConfigComponent       = ThemeConfigCategory<"Component", FunctionComponent, ThemeConfigComponentKeys>;

type ThemeConfigNumber          = ThemeConfigCategory<"Number", number, ThemeConfigNumberKeys>;

/** 
 * Flat object similar to a basic INI file 
 */
export type ThemeConfig = 
    ThemeConfigStrings 
    & ThemeConfigStyleClasses 
    & ThemeConfigStyles 
    & ThemeConfigBoolean 
    & ThemeConfigObject 
    & ThemeConfigImage 
    & ThemeConfigComponent
    & ThemeConfigNumber;

type ValuesOf<T> = T[keyof T];
type ThemeConfigValidKeysForPropKey<P, K extends keyof P> = Exclude<ValuesOf<{ [ConfigKey in keyof ThemeConfig]: ThemeConfig[ConfigKey] extends P[K] ? ConfigKey : never }>, never>;

export type ThemeConfigPropMapObject<P> = Partial<{ [PropKey in keyof P]: ThemeConfigValidKeysForPropKey<P, PropKey> }>;

export const ThemeContext = createContext<ThemeConfig>({});

/** Function to build the context provider component to be used at app root */
export const buildThemeWrapper =
    (theme: ThemeConfig) =>
        (props: Omit<ProviderProps<ThemeConfig>, "value">) =>
            <ThemeContext.Provider value={theme} {...props} />

/** Wrapper function to merge props via higher-order components */
export function applyThemeValuesToPropsHoc<P extends object>(map: ThemeConfigPropMapObject<P>, component: FunctionComponent<P>, onlyApplyWhenUndefined: boolean = false) {

    const Component = component;

    const WrappedComponent = React.forwardRef<typeof Component, P>((props, ref) => {
        const theme = useContext(ThemeContext);

        // Use a type assertion here
        const newProps = { ...props } as P;

        (Object.entries(map) as [keyof P, keyof ThemeConfig][]).forEach(([propKey, themeConfigKey]) => {
            if (!onlyApplyWhenUndefined || (props as any)[propKey] === undefined) {
                (newProps as any)[propKey] = theme[themeConfigKey];
            }
        });

        return <Component ref={ref} {...newProps} />;
    });

    // Apply readable name
    WrappedComponent.displayName = `ThemeValuesHoc(${Component.displayName ?? Component.name ?? "?"})`;

    return WrappedComponent;
}

/** Function to merge two objects of classnames */
const composeClassnames = <T extends Record<string, string>>(target: T, source: T) => {
    // Copy target out and merge classnames from source
    const newTarget = { ...target };

    for (const [key, sourceClassname] of (Object.entries(source) as [keyof T, T[keyof T]][])) {
        // Inject new entry if it does not already exist
        if (newTarget[key] === undefined) {
            newTarget[key] = sourceClassname;
            continue;
        }

        // Otherwise merge the two classnames
        newTarget[key] = classnames(newTarget[key], sourceClassname) as T[keyof T];
    }

    return newTarget;
}

/** Hook to apply theme from within component */
export const useTheme = () => {
    const theme = useContext(ThemeContext);

    const getThemeValue = useCallback(
        <K extends keyof ThemeConfig, V = ThemeConfig[K]>(key: K) => theme[key] as unknown as (V | undefined),
        [theme]
    );

    const applyThemeValuesToProps = useCallback(
        <P extends object>(map: ThemeConfigPropMapObject<P>, props: P, onlyApplyWhenUndefined: boolean = false): P => {
            const newProps = { ...props };

            for (const [propKey, themeConfigKey] of (Object.entries(map) as [keyof P, keyof ThemeConfig][])) {
                // Only apply theme value when the `onlyApplyWhenUndefined`
                // option is false, or if the value in `props` is indeed
                // `undefined`
                if (!onlyApplyWhenUndefined || props[propKey] === undefined) {
                    newProps[propKey] = getThemeValue(themeConfigKey) as P[keyof P];
                }
            }

            return newProps;
        },
        [getThemeValue]
    );

    const mergeStyles = useCallback(
        (originalStyle: Record<string, string>, themeStyle: keyof ThemeConfigStyles) => composeClassnames(originalStyle, getThemeValue(themeStyle) ?? {}),
        [getThemeValue]
    );

    const mergeStyleClasses = useCallback(
        (originalClasses: string | readonly string[], themeClasses: (keyof ThemeConfigStyleClasses)[]) =>
            // Error TS2345 being generated in recent builds, was fine prev :\
            // @ts-ignore  
            classnames(originalClasses, themeClasses.map(c => getThemeValue(c))),
        [getThemeValue]
    );

    return {
        getThemeValue,
        applyThemeValuesToProps,
        mergeStyleClasses,
        mergeStyles,
        theme,
    }
}
