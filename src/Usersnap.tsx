import getConfig from "next/config";

const config = getConfig();

export interface Props {
    url?: string | undefined;
}

export function Usersnap({
    url
}: Props) {

    if (url === undefined){
        if (config.publicRuntimeConfig.NEXT_PUBLIC_USERSNAP_URL !== undefined) {
            url = config.publicRuntimeConfig.NEXT_PUBLIC_USERSNAP_URL as string;
        } else {
            url = "https://widget.usersnap.com/global/load/e319db49-0f98-4f2d-8c43-2e847dfeadbe?onload=onUsersnapCXLoad";
        }
    }

    return (
        <script dangerouslySetInnerHTML={{
            // Usersnap script
            __html: `
            if (new URLSearchParams(document.location.search).get("embed") !== "1"){
                window.onUsersnapCXLoad = function(api) {
                    api.init();
                }
                var script = document.createElement("script");
                script.defer = 1;
                script.src = "${url}";
                document.getElementsByTagName("head")[0].appendChild(script);
            }`,
        }}
    />
    );
};
