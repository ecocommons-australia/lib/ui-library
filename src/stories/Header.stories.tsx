import React, { ComponentProps } from "react";
import { Story, Meta } from "@storybook/react";

import { Header } from "../Header";

export default {
    title: "Header",
    component: Header,
} as Meta;

const Template: Story<ComponentProps<typeof Header>> = (args) => (
    <Header {...args} />
);

export const Default = Template.bind({});
Default.args = {};

export const ActiveTab = Template.bind({});
ActiveTab.args = {
    activeTab: "data",
};

export const ActiveTabWithSubbar = Template.bind({});
ActiveTabWithSubbar.args = {
    activeTab: "functions",
    tabLinks: {
        ECOCOMMONS_ROOT: "https://root.example.com",
        ECOCOMMONS_DATA_VISUALISATIONS: "https://data.example.com",
        ECOCOMMONS_TOOLS_FUNCTIONS: "https://tools.example.com",
        ECOCOMMONS_VIRTUAL_LABORATORIES: "https://labs.example.com",
    },
    subBarLinks: [
        {
            key: "first",
            label: "First subbar link",
            href: "/first-subbar-link",
            align: "left"
        },
        {
            key: "second",
            label: "Second (active) subbar link",
            href: "/second-subbar-link",
            align: "left"
        },
        {
            key: "third",
            label: "Third subbar link",
            href: "/third-subbar-link",
            align: "right"
        },
        {
            key: "forth",
            label: "Forth subbar link",
            href: "/Forth-subbar-link",
            align: "right"
        },
    ],
    subBarActiveKey: "second",
};
