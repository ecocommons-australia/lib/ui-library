import React, { ComponentProps } from "react";
import { Story, Meta } from "@storybook/react";

import { FixedContainer } from "../FixedContainer";

export default {
    title: "FixedContainer",
    component: FixedContainer,
} as Meta;

const Template: Story<ComponentProps<typeof FixedContainer>> = (args) => (
    <FixedContainer {...args}>
        This component is a basic non-responsive container with a fixed width.
        <br />
        <br />
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin bibendum
        vestibulum nisl, id accumsan est finibus id. Morbi convallis rutrum
        ligula non mattis. Nunc erat velit, facilisis at purus et, vulputate
        iaculis erat. Nunc tempor purus vitae aliquet gravida. In sodales augue
        eget ipsum luctus, ut luctus dolor dignissim. Sed condimentum sit amet
        libero a vulputate. Quisque mattis mattis ante id porttitor. Aliquam non
        erat quis nibh mattis maximus. Vivamus id metus id arcu porta tristique.
        Vivamus vitae ante sed leo molestie porta. Fusce justo ante, vestibulum
        rutrum urna eu, aliquet ultricies nunc. Ut eleifend mi non neque
        aliquam, eget imperdiet leo placerat. Donec velit erat, tincidunt ut
        aliquet vel, imperdiet a velit. Mauris cursus magna eu lectus iaculis,
        eget porttitor leo dapibus. Phasellus sollicitudin est turpis, sed
        placerat tortor ultrices volutpat.
    </FixedContainer>
);

export const Default = Template.bind({});
Default.args = {};
