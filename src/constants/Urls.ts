export const Urls = {
    ECOCOMMONS_ROOT: "https://app.ecocommons.org.au",
    ECOCOMMONS_WORKSPACE:
        "https://workspace.app.ecocommons.org.au",
    ECOCOMMONS_DATASETS: "https://data-explorer.app.ecocommons.org.au",
    ECOCOMMONS_ANALYSIS_HUB: "https://analysis-tools.app.ecocommons.org.au",
    ECOCOMMONS_SUPPORT: "https://support.ecocommons.org.au",
};
