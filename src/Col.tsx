import classnames from "classnames";
import { Col as RGSCol } from "react-grid-system";
import { ComponentPropsWithoutRef } from "react";

import style from "./Col.module.css";

interface CustomProps {
    /** Whether to disable vertical growing of the column */
    disableFlexGrow?: boolean;
}

export function Col({
    className,
    disableFlexGrow = false,
    ...props
}: ComponentPropsWithoutRef<typeof RGSCol> & CustomProps) {
    // TODO: Restore `ref` passthrough
    return (
        <RGSCol
            className={classnames(
                { [style.disableFlexGrow]: disableFlexGrow },
                className
            )}
            {...props}
        />
    );
}
