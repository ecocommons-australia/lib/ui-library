import React from "react";
import classnames from "classnames";

import styles from "./styles/FixedContainer.module.css";

/**
 * A fixed-width non-responsive content container.
 */
export function FixedContainer({
    className,
    ...props
}: React.DetailedHTMLProps<
    React.HTMLAttributes<HTMLDivElement>,
    HTMLDivElement
>) {
    return (
        <div
            className={classnames(styles.bodyContainer, className)}
            {...props}
        />
    );
}
