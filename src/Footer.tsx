import { ReactElement } from "react";
import { useTheme } from "./Theme";
import { Container, Col, Row } from "react-grid-system";
import styles from "./styles/Footer.module.css";

import ecLogoImage from "./images/EcoCommons Logo White.png";
import nectarLogoImage from "./images/logo_nectar.png";

const FooterContentDefault = <>
    <Row justify="center" style={{alignItems: 'end'}}>
        <Col sm={6} md={2}  style={{position:'relative', top:'0.4rem'}}>
            <a href="https://www.ecocommons.org.au/">
                <img className="ec_logo" style={{width:'10rem'}} 
                    src={ecLogoImage}
                    alt="EcoCommons"
                    />
            </a>
        </Col>
        <Col sm={6} md={1}>
            <ul>
                <li key="about-us"><a href="https://www.ecocommons.org.au/about/">About</a></li> 
            </ul>
        </Col>
        <Col sm={6} md={1}>
            <ul>
                <li key="support"><a href="https://support.ecocommons.org.au">Support</a></li> 
            </ul>
        </Col>
        <Col sm={6} md={2}>
            <ul>
                <li key="contact-us"><a href="https://www.ecocommons.org.au/contact/">Contact</a></li> 
            </ul>
        </Col>
        <Col sm={6} md={2}>
        </Col>
        <Col sm={6} md={2} style={{maxHeight:'50px'}}>
            <a href="https://ardc.edu.au/services/ardc-nectar-research-cloud/"
               style={{position:'relative', top:'1rem'}}>
                <div style={{fontSize: '80%'}}>Powered by</div>
                <img className="nectar_logo" 
                    style={{width:'9rem', paddingTop:'0'}} 
                    src={nectarLogoImage} 
                    alt="ARDC Nectar Research Cloud"
                    />
            </a>
        </Col>

    </Row>
    <Row justify="center" style={{paddingTop: '1rem', paddingBottom: '0.75rem', fontSize: '80%'}}>
        <Col sm={6} md={6}>
            Copyright © 2024 The Queensland Cyber Infrastructure Foundation. All Rights Reserved.
        </Col>
    </Row>
</>;

export function Footer() {
    const { getThemeValue, mergeStyles } = useTheme();
    const themedStyles = mergeStyles(styles, "Styles::Footer");
    const themedContent: ReactElement | undefined = getThemeValue("Object::Platform.FooterContent");

    return (
        <footer>
            <Container fluid className={themedStyles.footer}>
               {themedContent ?? FooterContentDefault}
            </Container>
        </footer>
    );
}