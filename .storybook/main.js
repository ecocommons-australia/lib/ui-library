module.exports = {
    stories: [
        "../src/**/*.stories.mdx",
        "../src/**/*.stories.@(js|jsx|ts|tsx)",
    ],
    addons: ["@storybook/addon-links", "@storybook/addon-essentials"],
    webpackFinal: async (config, { configType }) => {
        // Get index of CSS rule
        const ruleCssIndex = config.module.rules.findIndex(
            (rule) => rule.test.toString() === "/\\.css$/"
        );

        // Tell this rule to exclude `*.module.css`
        config.module.rules[ruleCssIndex].exclude = /\.module\.css$/;

        // Push new rule before old CSS rule
        config.module.rules.splice(ruleCssIndex - 1, 0, {
            test: /\.css$/,
            use: [
                "style-loader",
                {
                    loader: "css-loader",
                    options: {
                        importLoaders: 1,
                        modules: {
                            mode: "local",
                            localIdentName:
                                configType === "PRODUCTION"
                                    ? "[local]--[hash:base64:5]"
                                    : "[name]__[local]--[hash:base64:5]",
                        },
                    },
                },
            ],
            include: /\.module\.css$/,
        });

        // Return reference to altered config
        return config;
    },
};
