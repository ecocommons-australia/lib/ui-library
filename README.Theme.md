# @ecocommons-australia/ui-library

# Theme
Theme provides an abstraction for specific branding, toggling features, text descriptions, style and layout of 'themed' Component. 

It supports the reuse of core platform frontends over multiple projects that provide different domain 'lenses' and it provides a way to generally abstract and consolidate configuration accross multiple frontend services.

## Application implementation

Theme is implemented using the React Context `<ThemeWrapper>` at the application root.  Theme parameter values are available to the entire application scope via a hook `useTheme`.

A default application Theme should be defined at `/themes/default/theme.ts` that implements the `ThemeConfig` interface (see `Theme.tsx`).
This can be overrwritten at build time or by using `docker-compose.yml` for local dev.

```
import { buildThemeWrapper } from "@ecocommons-australia/ui-library";

import { theme } from "../themes/default/theme";
import "../themes/default/styles/global.css";

const ThemeWrapper = buildThemeWrapper(theme);

function MyApp({}) {
    return (
		<ThemeWrapper>
			<Application />
		</ThemeWrapper>
    );
}

```

Component using a theme value example.

```
import { useTheme } from "@ecocommons-australia/ui-library";

function MyComponent() {
	const { getThemeValue } = useTheme();

	const themedTitle = getThemeValue("Object::Platform.Title") ?? "default title";
}

```

## Guidelines for Theme authoring

Any part of an application could potentially be themed, but a few guiding principles should be followed. Ultimately if the themeing mechanism is over used or implemented inappropriately it will degrade application maintainability and erode the value proposition of themeing, ie maximising code reuse. If something needs to be themed too heavily it may be a candidate for a code fork.

- Generally, the less themed the better.  Collaborators should negotiate core functionality and presentation that follows a broard pattern of use, and only theme specific differences.
- When implementing a Theme override for a Component, the default behaviour should always be preserved as a fallback.
- A Theme should implement overrides with an identical interface to the original Component defined behaviour, this is especially important for type `Object::`.
- A Theme should never contain or duplicate actual application logic.  If specific behaviours are required then application Components should parameterize this behaviour and the Theme should only enable it.
- Themeing entire Components is permitted, but should be used sparingly and generally only for presentation, ie a landing page. 
  - If a Component takes any arguments, has side effects, callbacks, state etc, it should not be themed. 



## Limitations of Theme
- Enforcement of type `Object::` is not guarenteed by the Theme mechanism itself.  The responsiblity is on the Component.  This should be improved by exposing Theme interfaces in the UILib.
- Theme is not a magic way to mesh complex logic into an application build.  It should be used for simple overrides and provide declarative structures that configure Component behaviours rather reimplementing them. There are other ways to modularize apps such as https://webpack.js.org/concepts/module-federation/



