#!/usr/bin/env bash

# Install packages
npm ci
# Need to also install peer dependencies!
npm i --no-save @blueprintjs/core classnames next react react-grid-system

npm run build
