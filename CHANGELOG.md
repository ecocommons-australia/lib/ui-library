## [1.6.17]
- Add theme key `Platform.SupportMap`

## [1.6.16]
- Add theme key `Workspace.Projects.Description`

## [1.6.14]
- Add theme key `Workflow.Projects.Owned.Description`
- Add theme key `Workflow.Projects.Shared.Description`
- Add theme key `Workflow.Projects.Shared.Description`

## [1.6.13]
- Add theme key `Object::Platform.HeaderTabLabels`
- Add theme key type `Component::` (React.FunctionComponent)
- Add theme key `Component::Workspace.Dashboard`
- Add theme key `Component::BccvlJobComposer.IndexPage`
- Add theme key `Bool::BccvlJobComposer.EnableBccvlSubBar`
- Add theme key `Bool::DataExplorer.Explore.EnableKnowledgeNetwork`
- Remove deprecated theme key `Map::`

## [1.6.11] - 2023-01-16
- Add BuildID component to Header (show build when NEXT_PUBLIC_BUILD_ID present)
- Add `env.ts` with `get**Url()` functions to provide a centralized single source of truth
